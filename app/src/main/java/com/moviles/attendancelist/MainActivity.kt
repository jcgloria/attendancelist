package com.moviles.attendancelist

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream

class MainActivity : AppCompatActivity() {

    val REQUEST_IMAGE_CAPTURE = 1
    var photo: Bitmap? = null
    var FOTO_TOMADA = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun takePhoto(view: View){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            photo = imageBitmap
            FOTO_TOMADA = true
        }
    }

    fun upload(view:View) {
        var fileName = editText.text.toString()
        if (!FOTO_TOMADA) {
            textView.text = "Error: No se ha tomado una foto"
        } else {
            if (!(fileName == null || fileName == "")) {
                val storage = FirebaseStorage.getInstance()
                var storageRef = storage.reference
                var ref = storageRef.child("images/" + fileName + ".jpg")
                val baos = ByteArrayOutputStream()
                photo?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                var uploadTask = ref.putBytes(data)
                uploadTask.addOnFailureListener {
                    textView.text = uploadTask.exception?.message
                }.addOnSuccessListener {
                    textView.text = "El archivo fue subido con exito"
                }
            } else{
                textView.text = "Error: Nombre de archivo vacio"
            }
        }
    }
}
